# Introduction

Slippi Stats Web is a web application to visualize data from Slippi replay game files (.slp) through various kind of charts. You can check your winrate with and/or against any character, on any stage, etc. You can also track your evolution through time for a specific matchup.

You can install it on your local machine or on a server by following instructions given below.

You can find a public demo on [slippi.mickaelgillot.xyz](https://slippi.mickaelgillot.xyz)

# Installation

## Installation with binaries (for Windows only)

Go to https://gitlab.com/Coukaratcha/slippi-stats-web/-/releases and download the Windows binaries for the latest release.

Unzip files in any directory of your choice.

Edit `ssw/config.cfg` and set the following variables:

* `PLAYER_CODE`: Enter your player's code.
* `REPLAYS`: Enter the fullpath of your replays directory (.slp files). **Note**: You must double the backslashes in paths for Windows. For example, `C:\Users\Couka\Documents\replays` becomes `C:\\Users\\Couka\\Documents\\replays`
* `LOCAL_ONLY`: Set "True" if you run this app locally. If you run this app on a server opened to the internet, set to "False" and set the two following variables:
	* `SECRET_KEY`: Enter a random string, you can use a password generator. It is used for login authentication. 
	* `PASSWORD`: Enter a password of your choice. It is the password you will use to login and access admin section.

Once it is done, just run `slippi-stats-web.exe` by double-clicking on it and go to http://localhost:5000 with your web browser.

At this point, files are not imported yet, so there is no data to visualize. To import your data, just go to http://localhost:5000/import and follow instructions given on this page.

If you wrongly imported some files you do not want in your data, go to http://localhost:5000/edit then find and select files you want to delete from the database.

### Troubleshooting

It may happens that your antivirus detects the exe as a malware/virus. To fix this, you can indicate to your antivirus to make an exception for the directory where you unzip files. For more information about how to add an exception, [here](https://www.howtogeek.com/671233/how-to-add-exclusions-in-windows-defender-on-windows-10/) are instructions for Windows Defender (default antivirus on Windows 10)

## Alternatives

### Installation with Docker

First fetch the whole project, either by cloning it or just download it as an archive and extract it.

```
$ git clone git@gitlab.com:Coukaratcha/slippi-stats-web.git
```  

or

```
$ wget https://gitlab.com/Coukaratcha/slippi-stats-web/-/archive/master/slippi-stats-web-master.zip && unzip slippi-stats-web-master.zip
```

Then you have to create some folders and copy `production.env.sample` to `production.env`. For this, just run `init.sh` script.

```
$ . ./init.sh
```

Once `production.env` is created, you must edit it and set some variables for your instance.

First enter your slippi player's code into `PLAYER_CODE`. It should be like XXXX#123, for example mine is CKA#918. 

Then choose if your instance will only be runned locally, or accessible over internet. If you open your instance to internet, you must set a password in `PASSWORD` (used to protect access ) and a secret key in `SECRET_KEY`. You can use a password generator to get a strong password and secret key.

Now, configuration is done. You are ready!

Run the server with

```
$ docker-compose up -d
```

go to http://localhost:5000 with your web browser.

At this point, files are not imported yet, so there is no data to visualize. To import your data, copy `.slp` files to `replays/` folder, go to http://localhost:5000/import and follow instructions given on this page.

If you wrongly imported some files you do not want in your data, go to http://localhost:5000/edit then find and select files you want to delete from the database.

### Installation without Docker

To make Slippi Stats Web work, you will need to have [Python 3](https://www.python.org/downloads/) installed on your computer.

Fetch the project's files using one of the commands given with Docker installation tutorial.

Copy `/web/app/ssw/config.cfg.sample` into `/web/app/ssw/config.cfg` then edit it and set the following variables:

* `PLAYER_CODE`: Enter your player's code.
* `REPLAYS`: Enter the fullpath of your replays directory (.slp files). **Note**: You must double the backslashes in paths for Windows. For example, `C:\Users\Couka\Documents\Slippi` becomes `C:\\Users\\Couka\\Documents\\Slippi`
* `LOCAL_ONLY`: Set "True" if you run this app locally. If you run this app on a server opened to the internet, set to "False" and set the two following variables:
	* `SECRET_KEY`: Enter a random string, you can use a password generator. It is used for login authentication. 
	* `PASSWORD`: Enter a password of your choice. It is the password you will use to login and access admin section.

Then open a terminal, go to `web/app/` and run:

```
$ pip install -r requirements.txt
```

Finally run the server using the command:

```
$ python run.py
```

go to http://localhost:5000 with your web browser.

At this point, files are not imported yet, so there is no data to visualize. To import your data, just go to http://localhost:5000/import and follow instructions given on this page.

If you wrongly imported some files you do not want in your data, go to http://localhost:5000/edit then find and select files you want to delete from the database.
