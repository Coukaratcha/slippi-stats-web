from ssw.database import db

from datetime import datetime

class Game(db.Model):
	__tablename = 'game'
	filename = db.Column(db.String(255), primary_key=True)
	status = db.Column(db.String(20), nullable=False)
	datetime = db.Column(db.DateTime, nullable=False)
	opponent = db.Column(db.String(20), nullable=False)
	opponent_nickname = db.Column(db.String(255), nullable=False)
	char_player = db.Column(db.String(20), nullable=False)
	char_opponent = db.Column(db.String(20), nullable=False)
	score_player = db.Column(db.Integer, nullable=False)
	score_opponent = db.Column(db.Integer, nullable=False)
	win = db.Column(db.Boolean)
	stage = db.Column(db.String(255))
	duration = db.Column(db.Integer, nullable=False)

	@property
	def json(self):
		return {
            'filename': self.filename,
			'datetime': self.datetime,
			'opponent': self.opponent,
            'opponent_nickname': self.opponent_nickname,
			'char_player': self.char_player,
			'char_opponent': self.char_opponent,
			'score_player': self.score_player,
			'score_opponent': self.score_opponent,
			'win': self.win,
			'stage': self.stage,
            'duration': self.duration
		}
