from flask import (
	Flask,
	render_template,
	redirect,
	request,
	Blueprint,
	current_app,
	session,
	url_for
)

import sys, os

from ssw.database import db
from ssw.api import api

from ssw.utils import require_login

app = Flask(__name__)
app.config.from_object('ssw.config.Config')	
app.config.from_pyfile(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.cfg'), silent = True)
app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///' + os.path.join(app.config["DATA"], "slippi-stats.db")

app.register_blueprint(api)

db.init_app(app)

with app.app_context():
	db.create_all()

@app.route('/')
@app.route('/overall')
def home():
	return render_template('index.html', player=app.config['PLAYER_CODE'])

@app.route('/evolution')
def evolution():
	return render_template('evolution.html', player=app.config['PLAYER_CODE'])

@app.route('/matchups')
def matchups():
	return render_template('matchups.html', player=app.config['PLAYER_CODE'])

@app.route('/edit')
@require_login
def edit():
	return render_template('edit.html')

@app.route('/import')
@require_login
def import_data():
	return render_template('import.html', player=app.config['PLAYER_CODE'], replays=app.config['REPLAYS'])

@app.route('/about')
def about():
	return render_template('about.html')

@app.route('/login', methods=["GET", "POST"])
def login():
	if request.method == "GET":
		return redirect(url_for("home"))

	if current_app.config["LOCAL_ONLY"]:
		return redirect(url_for("home"))

	data = request.form
	
	if "password" in data and data["password"] == current_app.config["PASSWORD"]:
		session["login"] = True
		return redirect(url_for("home"))
		
	return redirect(url_for("home", login_error="Invalid credentials"))

@app.route('/logout')
def logout():
	if current_app.config["LOCAL_ONLY"]:
		return redirect(url_for("home"))

	session.pop("login", None)
	return redirect(url_for("home"))