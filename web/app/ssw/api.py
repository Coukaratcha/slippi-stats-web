from flask import (
	Blueprint,
	abort,
	jsonify,
	request,
	current_app
)

import os, datetime

import slippi

from sqlalchemy import or_, func, case
from sqlalchemy.orm import aliased

from ssw.models.game import Game
from ssw.database import db
from ssw.const import (
	characters,
	CSSCharacter,
	stages
)
from ssw.import_data import ImportingThread

from ssw.utils import require_login

api = Blueprint('api', __name__, template_folder='templates', url_prefix='/api')
imp = ImportingThread()

@api.route('/games', methods=["GET", "DELETE"])
def games():
	if request.method == "GET":
		games = Game.query
		req = request.args

		res = {
			"filter": {
				'opponent': req['opponent'] if 'opponent' in req and req['opponent'] else None,
				'with': req.getlist('with[]') if 'with[]' in req else [],
				'against': req.getlist('against[]') if 'against[]' in req else [],
				'stages': req.getlist('stages[]') if 'stages[]' in req else [],
				'date_before': req['date_before'] if 'date_before' in req and req['date_before'] else None,
				'date_after': req['date_after'] if 'date_after' in req and req['date_after'] else None
			}
		}

		if res['filter']['opponent']:
			games = games.filter(Game.opponent == res['filter']['opponent'])
		if res['filter']['with']:
			games = games.filter(Game.char_player.in_(res['filter']['with']))
		if res['filter']['against']:
			games = games.filter(Game.char_opponent.in_(res['filter']['against']))
		if res['filter']['stages']:
			games = games.filter(Game.stage.in_(res['filter']['stages']))
		if res['filter']['date_before']:
			games = games.filter(Game.datetime <= res['filter']['date_before'])
		if res['filter']['date_after']:
			games = games.filter(Game.datetime >= res['filter']['date_after'])

		games = games.order_by(Game.datetime.desc()).all()

		return jsonify([g.json for g in games])
	elif request.method == "DELETE":
		filenames = request.form.getlist("filenames[]")

		games = Game.query.filter(Game.filename.in_(filenames))
		games.delete(synchronize_session=False)
		db.session.commit()

		return jsonify([g.json for g in games])

@api.route('/matchups')
def matchups():
	player = current_app.config['PLAYER_CODE']

	games = [z.json for z in Game
		.query
		.filter(Game.win.isnot(None))
	]

	matchups = [ {'with': x, 'against': y, 'wins': 0, 'losses': 0, 'winrate': 0.5 } for y in characters for x in characters ]

	for g in games:
		m = [m for m in matchups if m['with'] == g['char_player'] and m['against'] == g['char_opponent']][0]

		if g['win']:
			m['wins']+= 1
		else:
			m['losses']+= 1

	for m in matchups:
		m['winrate'] = m['wins'] / (m['wins'] + m['losses']) if (m['wins'] + m['losses']) != 0 else 0.5 

	return jsonify(matchups)

@api.route('/overall')
def overall():
	player = current_app.config['PLAYER_CODE']
	req = request.args
	res = {
		'filter': {
			'player': player,
			'opponent': req['opponent'] if 'opponent' in req and req['opponent'] else None,
			'with': req.getlist('with[]') if 'with[]' in req else [],
			'against': req.getlist('against[]') if 'against[]' in req else [],
			'stages': req.getlist('stages[]') if 'stages[]' in req else [],
			'date_before': req['date_before'] if 'date_before' in req and req['date_before'] else None,
			'date_after': req['date_after'] if 'date_after' in req and req['date_after'] else None,
			'unique': req['unique'] == "true" if 'unique' in req and req['unique'] else False
		}
	}

	if res['filter']['unique']:
		win_set = case([
					(func.count().filter(Game.win == True) > func.count().filter(Game.win == False), True),
					(func.count().filter(Game.win == True) < func.count().filter(Game.win == False), False)
				],
				else_=None
			)

		global_data_sub = Game.query.with_entities(
			win_set.label("win")
		)
		with_data_sub = Game.query.with_entities(
			Game.char_player.label("character"),
			win_set.label("win")
		)
		against_data_sub = Game.query.with_entities(
			Game.char_opponent.label("character"),
			win_set.label("win")
		)
		stages_data_sub = Game.query.with_entities(
			Game.stage.label("stage"),
			win_set.label("win")
		)

		if res['filter']['opponent']:
			global_data_sub = global_data_sub.filter(Game.opponent == res['filter']['opponent'])
			with_data_sub = with_data_sub.filter(Game.opponent == res['filter']['opponent'])
			against_data_sub = against_data_sub.filter(Game.opponent == res['filter']['opponent'])
			stages_data_sub = stages_data_sub.filter(Game.opponent == res['filter']['opponent'])
		if res['filter']['with']:
			global_data_sub = global_data_sub.filter(Game.char_player.in_(res['filter']['with']))
			with_data_sub = with_data_sub.filter(Game.char_player.in_(res['filter']['with']))
			against_data_sub = against_data_sub.filter(Game.char_player.in_(res['filter']['with']))
			stages_data_sub = stages_data_sub.filter(Game.char_player.in_(res['filter']['with']))
		if res['filter']['against']:
			global_data_sub = global_data_sub.filter(Game.char_opponent.in_(res['filter']['against']))
			with_data_sub = with_data_sub.filter(Game.char_opponent.in_(res['filter']['against']))
			against_data_sub = against_data_sub.filter(Game.char_opponent.in_(res['filter']['against']))
			stages_data_sub = stages_data_sub.filter(Game.char_opponent.in_(res['filter']['against']))
		if res['filter']['stages']:
			global_data_sub = global_data_sub.filter(Game.stage.in_(res['filter']['stages']))
			with_data_sub = with_data_sub.filter(Game.stage.in_(res['filter']['stages']))
			against_data_sub = against_data_sub.filter(Game.stage.in_(res['filter']['stages']))
			stages_data_sub = stages_data_sub.filter(Game.stage.in_(res['filter']['stages']))
		if res['filter']['date_before']:
			global_data_sub = global_data_sub.filter(Game.datetime <= res['filter']['date_before'])
			with_data_sub = with_data_sub.filter(Game.datetime <= res['filter']['date_before'])
			against_data_sub = against_data_sub.filter(Game.datetime <= res['filter']['date_before'])
			stages_data_sub = stages_data_sub.filter(Game.datetime <= res['filter']['date_before'])
		if res['filter']['date_after']:
			global_data_sub = global_data_sub.filter(Game.datetime >= res['filter']['date_after'])
			with_data_sub = with_data_sub.filter(Game.datetime >= res['filter']['date_after'])
			against_data_sub = against_data_sub.filter(Game.datetime >= res['filter']['date_after'])
			stages_data_sub = stages_data_sub.filter(Game.datetime >= res['filter']['date_after'])

		global_data_sub = global_data_sub.group_by(Game.opponent).subquery()
		with_data_sub = with_data_sub.group_by(Game.opponent, Game.char_player).subquery()
		against_data_sub = against_data_sub.group_by(Game.opponent, Game.char_opponent).subquery()
		stages_data_sub = stages_data_sub.group_by(Game.opponent, Game.stage).subquery()
		
		global_data = Game.query.with_entities(
			func.count().label("games"),
			func.count().filter(global_data_sub.c.win == True).label("wins"),
			func.count().filter(global_data_sub.c.win == False).label("losses"),
			func.count().filter(global_data_sub.c.win == None).label("ties"),
			(func.count().filter(global_data_sub.c.win == True) * 100.0 / func.count().filter(global_data_sub.c.win.isnot(None))).label("winrate")
		).first()
		with_data = Game.query.with_entities(
			with_data_sub.c.character.label("character"),
			func.count().label("games"),
			func.count().filter(with_data_sub.c.win == True).label("wins"),
			func.count().filter(with_data_sub.c.win == False).label("losses"),
			func.count().filter(with_data_sub.c.win == None).label("ties"),
			(func.count().filter(with_data_sub.c.win == True) * 100.0 / func.count().filter(with_data_sub.c.win.isnot(None))).label("winrate")
		).group_by(with_data_sub.c.character).all()
		against_data = Game.query.with_entities(
			against_data_sub.c.character.label("character"),
			func.count().label("games"),
			func.count().filter(against_data_sub.c.win == True).label("wins"),
			func.count().filter(against_data_sub.c.win == False).label("losses"),
			func.count().filter(against_data_sub.c.win == None).label("ties"),
			(func.count().filter(against_data_sub.c.win == True) * 100.0 / func.count().filter(against_data_sub.c.win.isnot(None))).label("winrate")
		).group_by(against_data_sub.c.character).all()
		stages_data = Game.query.with_entities(
			stages_data_sub.c.stage.label("stage"),
			func.count().label("games"),
			func.count().filter(stages_data_sub.c.win == True).label("wins"),
			func.count().filter(stages_data_sub.c.win == False).label("losses"),
			func.count().filter(stages_data_sub.c.win == None).label("ties"),
			(func.count().filter(stages_data_sub.c.win == True) * 100.0 / func.count().filter(stages_data_sub.c.win.isnot(None))).label("winrate")
		).group_by(stages_data_sub.c.stage).all()
	else:
		games = func.count()
		wins = func.count().filter(Game.win == True)
		losses = func.count().filter(Game.win == False)
		ties = func.count().filter(Game.win == None)
		winrate = func.count().filter(Game.win == True) * 100.0 / func.count().filter(Game.win.isnot(None))
		
		global_data = Game.query.with_entities(
			games.label("games"),
			wins.label('wins'),
			losses.label('losses'),
			ties.label('ties'),
			winrate.label('winrate')
		)
		with_data = Game.query.with_entities(
			Game.char_player.label("character"),
			games.label("games"),
			wins.label('wins'),
			losses.label('losses'),
			ties.label('ties'),
			winrate.label('winrate')
		)
		against_data = Game.query.with_entities(
			Game.char_opponent.label("character"),
			games.label("games"),
			wins.label('wins'),
			losses.label('losses'),
			ties.label('ties'),
			winrate.label('winrate')
		)
		stages_data = Game.query.with_entities(
			Game.stage.label("stage"),
			games.label("games"),
			wins.label('wins'),
			losses.label('losses'),
			ties.label('ties'),
			winrate.label('winrate')
		)

		if res['filter']['opponent']:
			global_data = global_data.filter(Game.opponent == res['filter']['opponent'])
			with_data = with_data.filter(Game.opponent == res['filter']['opponent'])
			against_data = against_data.filter(Game.opponent == res['filter']['opponent'])
			stages_data = stages_data.filter(Game.opponent == res['filter']['opponent'])
		if res['filter']['with']:
			global_data = global_data.filter(Game.char_player.in_(res['filter']['with']))
			with_data = with_data.filter(Game.char_player.in_(res['filter']['with']))
			against_data = against_data.filter(Game.char_player.in_(res['filter']['with']))
			stages_data = stages_data.filter(Game.char_player.in_(res['filter']['with']))
		if res['filter']['against']:
			global_data = global_data.filter(Game.char_opponent.in_(res['filter']['against']))
			with_data = with_data.filter(Game.char_opponent.in_(res['filter']['against']))
			against_data = against_data.filter(Game.char_opponent.in_(res['filter']['against']))
			stages_data = stages_data.filter(Game.char_opponent.in_(res['filter']['against']))
		if res['filter']['stages']:
			global_data = global_data.filter(Game.stage.in_(res['filter']['stages']))
			with_data = with_data.filter(Game.stage.in_(res['filter']['stages']))
			against_data = against_data.filter(Game.stage.in_(res['filter']['stages']))
			stages_data = stages_data.filter(Game.stage.in_(res['filter']['stages']))
		if res['filter']['date_before']:
			global_data = global_data.filter(Game.datetime <= res['filter']['date_before'])
			with_data = with_data.filter(Game.datetime <= res['filter']['date_before'])
			against_data = against_data.filter(Game.datetime <= res['filter']['date_before'])
			stages_data = stages_data.filter(Game.datetime <= res['filter']['date_before'])
		if res['filter']['date_after']:
			global_data = global_data.filter(Game.datetime >= res['filter']['date_after'])
			with_data = with_data.filter(Game.datetime >= res['filter']['date_after'])
			against_data = against_data.filter(Game.datetime >= res['filter']['date_after'])
			stages_data = stages_data.filter(Game.datetime >= res['filter']['date_after'])
	
		global_data = global_data.first()
		with_data = with_data.group_by(Game.char_player).all()
		against_data = against_data.group_by(Game.char_opponent).all()
		stages_data = stages_data.group_by(Game.stage).all()

	# Fill with characters and stages missing in result
	for c in characters:
		if c not in [w.character for w in with_data]:
			missing_char = db.Model()
			missing_char.character = c
			missing_char.games = 0
			missing_char.wins = 0
			missing_char.losses = 0
			missing_char.ties = 0
			missing_char.winrate = None
			with_data.append(missing_char)

	for c in characters:
		if c not in [a.character for a in against_data]:
			missing_char = db.Model()
			missing_char.character = c
			missing_char.games = 0
			missing_char.wins = 0
			missing_char.losses = 0
			missing_char.ties = 0
			missing_char.winrate = None
			against_data.append(missing_char)

	for s in stages.values():
		if s not in [s.stage for s in stages_data]:
			missing_stage = db.Model()
			missing_stage.stage = s
			missing_stage.games = 0
			missing_stage.wins = 0
			missing_stage.losses = 0
			missing_stage.ties = 0
			missing_stage.winrate = None
			stages_data.append(missing_stage)

	return jsonify({
		'filter': res['filter'],
		'global': {
			'games': global_data.games,
			'wins': global_data.wins,
			'losses': global_data.losses,
			'ties': global_data.ties,
			'winrate': global_data.winrate
		},
		"with": [{
			'character': w.character,
			'games': w.games,
			'wins': w.wins,
			'losses': w.losses,
			'ties': w.ties,
			'winrate': w.winrate
		} for w in with_data],
		"against": [{
			'character': a.character,
			'games': a.games,
			'wins': a.wins,
			'losses': a.losses,
			'ties': a.ties,
			'winrate': a.winrate
		} for a in against_data],
		"stages": [{
			'stage': s.stage,
			'games': s.games,
			'wins': s.wins,
			'losses': s.losses,
			'ties': s.ties,
			'winrate': s.winrate
		} for s in stages_data]
	})

@api.route('/evolution')
def evolution():
	player = current_app.config['PLAYER_CODE']
	req = request.args
	res = {
		'filter': {
			'player': player,
			'opponent': req['opponent'] if 'opponent' in req and req['opponent'] else None,
			'with': req.getlist('with[]') if 'with[]' in req else [],
			'against': req.getlist('against[]') if 'against[]' in req else [],
			'stages': req.getlist('stages[]') if 'stages[]' in req else [],
			'date_before': req['date_before'] if 'date_before' in req and req['date_before'] else None,
			'date_after': req['date_after'] if 'date_after' in req and req['date_after'] else None
		}
	}

	date = func.DATE(Game.datetime).label('date')
	month = func.strftime('%Y-%m', Game.datetime).label('month')

	Game2 = aliased(Game, name="g2")

	games = Game2.query.with_entities(func.count().filter(Game2.win.isnot(None)))
	wins = Game2.query.with_entities(func.count().filter(Game2.win))

	if res['filter']['opponent']:
		games = games.filter(Game2.opponent == res['filter']['opponent'])
		wins = wins.filter(Game2.opponent == res['filter']['opponent'])
	
	if res['filter']['with']:
		games = games.filter(Game2.char_player.in_(res['filter']['with']))
		wins = wins.filter(Game2.char_player.in_(res['filter']['with']))
	
	if res['filter']['against']:
		games = games.filter(Game2.char_opponent.in_(res['filter']['against']))
		wins = wins.filter(Game2.char_opponent.in_(res['filter']['against']))

	if res['filter']['stages']:
		games = games.filter(Game2.stage.in_(res['filter']['stages']))
		wins = wins.filter(Game2.stage.in_(res['filter']['stages']))
	
	if res['filter']['date_before']:
		games = games.filter(Game2.datetime <= res['filter']['date_before'])
		wins = wins.filter(Game2.datetime <= res['filter']['date_before'])
	
	if res['filter']['date_after']:
		games = games.filter(Game2.datetime >= res['filter']['date_after'])
		wins = wins.filter(Game2.datetime >= res['filter']['date_after'])

	daily = Game.query.with_entities(
		date, 
		func.count().label('games_daily'), 
		func.count().filter(Game.win == True).label('wins_daily'), 
		games.filter(func.DATE(Game2.datetime) <= date).label('games'),
		wins.filter(func.DATE(Game2.datetime) <= date).label('wins')
	).filter(Game.win.isnot(None))

	monthly = Game.query.with_entities(
		month,
		games.filter(func.strftime('%Y-%m', Game2.datetime) == month).label('games'),
		wins.filter(func.strftime('%Y-%m', Game2.datetime) == month).label('wins')
	).filter(Game.win.isnot(None))
	
	if res['filter']['opponent']:
		daily = daily.filter(Game.opponent == res['filter']['opponent'])
		monthly = monthly.filter(Game.opponent == res['filter']['opponent'])
	if res['filter']['with']:
		daily = daily.filter(Game.char_player.in_(res['filter']['with']))
		monthly = monthly.filter(Game.char_player.in_(res['filter']['with']))
	if res['filter']['against']:
		daily = daily.filter(Game.char_opponent.in_(res['filter']['against']))
		monthly = monthly.filter(Game.char_opponent.in_(res['filter']['against']))
	if res['filter']['stages']:
		daily = daily.filter(Game.stage.in_(res['filter']['stages']))
		monthly = monthly.filter(Game.stage.in_(res['filter']['stages']))
	if res['filter']['date_before']:
		daily = daily.filter(Game.datetime <= res['filter']['date_before'])
		monthly = monthly.filter(Game.datetime <= res['filter']['date_before'])
	if res['filter']['date_after']:
		daily = daily.filter(Game.datetime >= res['filter']['date_after'])
		monthly = monthly.filter(Game.datetime >= res['filter']['date_after'])
	
	daily = daily.group_by('date')
	monthly = monthly.group_by('month')
			
	return jsonify({
		"filter": res['filter'],
		"daily": [{
			"date": z.date,
			"games_daily": z.games_daily,
			"games": z.games,
			"wins_daily": z.wins_daily,
			"wins": z.wins,
			"winrate_daily": z.wins_daily * 100.0 / z.games_daily,
			"winrate": z.wins * 100.0 / z.games} for z in daily],
		"monthly": [{
			"month": z.month,
			"games": z.games,
			"wins": z.wins} for z in monthly]
	})

@api.route('/parse')
@require_login
def parse():
	imp.exclude = request.args['exclude']
	if not imp.lock:
		imp.run()

	return jsonify(imp.progress)

@api.route('/progress')
def progress():
	return jsonify({
		"exclude": imp.exclude,
		"progress": imp.progress
	})
