let logs = []
let timer;

function getFilters(){
	return $('.log .filters button:not(.disabled)').map(function(){return $(this).text();}).toArray();
}

function displayLogs(){
	let filters = getFilters();

	$('.log .content').empty();

	for (x of logs.filter(e => filters.indexOf(e[0]) != -1)){
		let t = x[0], v = x[1];
		let color = 'text-body';

		if (t == 'SUCCESS'){
			color = 'text-success';
		}
		else if (t == 'INFO'){
			color = 'text-warning';
		}
		else if (t == 'ERROR'){
			color = 'text-danger';
		}

		let log = $('<span></span>')
			.addClass(color)
			.text(v)
			.append($('<br>'));

		$('.log .content').prepend(log)
	}
}

function update(data){
	const progress = data["progress"];
	let p = progress['imported'] + progress['ignored'] + progress['errors'];
	let t = progress['total'];
	let r = t > 0 ? 100 * p / t : 100.0;
	let filters = getFilters();
	logs = progress['log'];
	
	$('.progress-output .total').text(t);
	$('.progress-output .imported').text(progress['imported']);
	$('.progress-output .errors').text(progress['errors']);
	$('.progress-output .ignored').text(progress['ignored']);
	$('.progress-output .progression').text(r.toFixed(2) + '%');
	$('.progress-bar')
		.attr('aria-valuenow', 100 * p / t)
		.css('width', r + '%');
	
	logs = progress['log'];

	displayLogs();
}

$('.log .filters button').click(e => {
	let btn = $(e.target);
	btn
		.parent()
		.children()
		.removeClass('disabled');

	btn
		.nextAll()
		.addClass('disabled');

	displayLogs();
});

$.get('api/progress', data => {
	if (data['progress']['running']){
		$('form#import input#exclude').prop("checked", data["exclude"]);
		progress();
	}
});

function progress(){
	let btn = $('form#import button');

	btn.attr('disabled', true);
	$('form#import input').attr('disabled', true);

	timer = setInterval(() => {
		$.get('api/progress', data => {
			const progress = data["progress"];
			if (progress['imported']+progress['ignored']+progress['errors'] == progress['total']){
				clearTimeout(timer);
				btn.attr('disabled', false);
				$('form#import input').attr('disabled', false);
			}
			update(data);
		})
	}, 500);
}

$('form#import').submit(e => {
	e.preventDefault();
	$.get({
		url: 'api/parse',
		data: {
			exclude: $('form#import input#exclude').prop("checked")
		}
	});
	progress();
});
