function update(data){
	$('#loading > .spinner-border').hide();

	if (!data.length){
		$('#no-data').show();
		$('#data').hide();
		return;
	}
	
	$('#no-data').hide();
	$('#data').show();
	
	$(".count-games").text(data.length);
	$("table tbody").empty();
	$("h1 .count").text()
	for (g of data){
		$('table tbody')
			.append('<tr>'
			+ `<td><input type="checkbox" data-filename="${g.filename}"></td>`
			+ `<td>${g.filename}</td>`
			+ `<td>${new Date(g.datetime).toLocaleString('fr-FR', {})}</td>`
			+ `<td>${g.opponent} (${g.opponent_nickname})</td>`
			+ `<td><img src="/static/img/characters/${g.char_player}.png" title="${g.char_player}"> - <img src="/static/img/characters/${g.char_opponent}.png" title="${g.char_opponent}"></td>`
			+ `<td>${g.stage}</td>`
			+ `<td>${Math.floor(Math.round(g.duration/60)/60)}:${("0" + Math.round(g.duration/60)%60).slice(-2)}</td>`
			+ `<td><span class="badge badge-${g.win ? "success" : g.win == false ? "danger" : "warning"}">${g.win ? "Win" : g.win == false ? "Loss" : "Tie"}</span></td>` 
			+ '</tr>'
			);
	}

	$("table input[type=checkbox]").on("change", function(){
		const number = $("table tbody input[type=checkbox]:checked").length;
		$(".count-delete").text(`${number} ${number > 1 ? 'games' : 'game'}`);
	});
}

function submit(){
	$('#loading > .spinner-border').show();
	$('#data').hide();
	$('#no-data').hide();

	updateFilters();
	
	$.get({
		url: "/api/games",
		data: filters
	}).done(update);
}

$(function(){
	$('table input[type=checkbox]').prop("checked", false);
	fillForm();

	$('form#filters').on('submit', function(e){
		e.preventDefault();

		submit();
	});

	$("table thead input[type=checkbox]").on("change", function(){
		$("table tbody input[type=checkbox]").prop("checked", $(this).prop("checked"));
	});

	$("#modal-confirm-delete button.confirm").click(function(){
		const filenames = $.map($("table tbody input[type=checkbox]:checked"), e => $(e).attr("data-filename"));

		$.ajax({
			url: "/api/games",
			type: "DELETE",
			data: {
				filenames: filenames
			}
		})
		.done(function(){
			$('.notifications').append('<div class="alert alert-primary alert-dismissible fade show" role="alert">'
				+ `<strong>Success!</strong> You have just deleted ${filenames.length} ${filenames.length > 1 ? 'games' : 'game'}`
				+ '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
				+ '<span aria-hidden="true">&times;</span>'
				+ '</button>'
			+ '</div>');
			$('.alert').alert();
			submit();
		})
	});

	$('form#filters button#reset').click(function(e){
		e.preventDefault();

		$('form')
			.find('select')
			.prop('selectedIndex', -1);

		$('form input[type="text"]').val('');
		$('form input[type="checkbox"]').prop('checked', false);

		submit();
	});

	submit();
});