const margin = {
	top: 15,
	bottom: 50,
	left: 30,
	right: 30
}

const w = 800 - margin.left - margin.right;
const h = 800 - margin.top - margin.bottom;

function submit(){
	$('#loading > .spinner-border').show();
	$('#data').hide();
	$('#no-data').hide();
	
	updateFilters();

	$('#player-info .characters').empty();
	for(c of filters.with){
		$('#player-info .characters')
			.append($('<img>').attr("src", "/static/img/characters/"+c+".png"));
	}

	$('#opponent-info .characters').empty();
	for(c of filters.against){
		$('#opponent-info .characters')
			.append($('<img>').attr("src", "/static/img/characters/"+c+".png"));
	}
	
	$.get({
		url: "/api/evolution",
		data: filters
	}).done(update);
}

function update(data){
	$('#loading > .spinner-border').hide();

	if (!data.daily.length){
		$('#no-data').show();
		$('#data').hide();
		return;
	}
	
	$('#no-data').hide();
	$('#data').show();

	$('.match #player .code').text(data['filter']['player']);
	$('.match #opponent .code').text(data['filter']['opponent'] ? data['filter']['opponent'] : "?");
	
	const charts = [
		"#chart-games-time",
		"#chart-wins-time"
	];

	for (c of charts){
		$(c).children("svg").remove();
	}

	const tip = d3.select("body")
		.append("div")
		.attr("class", "tip");

	function show_tip(x, y){
		tip
			.style("opacity", 1)
			.style("top", `${y}px`)
			.style("left", `${x}px`)
			.style("visibility", "visible");
	}
	
	function hide_tip(){
		tip
			.style("opacity", 0)
			.style("visibility", "hidden");
	}

	// Line graph daily
	(() => {
		const _data = data["daily"].map( x => ({ 
			"date": new Date(x.date), 
			"winrate_daily": x.winrate, 
			"games_daily": x.games_daily, 
			"wins_daily": x.wins_daily,
			"games": x.games, 
			"winrate": x.winrate,
			"wins": x.wins
		}));

		const svg = d3.select("#chart-games-time")
			.append("svg")
			.attr("viewBox", "0 0 800 800")
			.append("g")
			.attr("transform", `translate(${margin.left}, ${margin.top})`);	
			
		const x = d3.scaleTime()
			.domain(d3.extent(_data, d => d.date))
			.range([0, w]);

		svg.append("g")
			.attr("transform", `translate(0, ${h})`)
			.call(d3.axisBottom(x));

		yExtent = d3.extent(_data, d => d.winrate);
		yExtent[0] = Math.floor(yExtent[0]);
		yExtent[1] = Math.ceil(yExtent[1]);

		const y = d3.scaleLinear()
			.domain(yExtent)
			.range([h, 0]);

		svg.append("g")
			.call(d3.axisLeft(y).tickFormat(d => d + '%'))
			.selectAll(".tick:not(:first-of-type)")
			.append("line")
				.attr("stroke", "black")
				.attr("x1", 0)
				.attr("x2", w)
				.attr("stroke-opacity", 0.5)
				.attr("stroke-dasharray", "2,2");
			
		svg.append("g")
			.attr("transform", `translate(${w}, 0)`)
			.call(d3.axisRight(y).tickFormat(d => d + '%'));


		svg.append("path")
			.datum(_data)
			.attr("fill", "#bfdeff")
			.style("fill-opacity", 0.7)
			.attr("stroke", "#007bff")
			.attr("stroke-width", 1.5)
			.attr("d", d3.area()
				.x(d => x(d.date))
				.y0(y(yExtent[0]))
				.y1(d => y(d.winrate)));
		
		svg.selectAll("circles")
			.data(_data)
			.enter()
			.append("circle")
				.attr("fill", "#3f9cff")
				.attr("stroke", "none")
				.attr("cx", d => x(d.date))
				.attr("cy", d => y(d.winrate))
				.attr("r", 3)
				.on("mouseover", function(d){
					tip.html(`${d.date.toLocaleDateString("en-GB")}<br><strong>${d.winrate.toFixed(2)}%</strong>`);
					const e = d3.select(this).node().getBoundingClientRect();
		
					const coords = {
						x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
						y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
					}
					show_tip(coords.x, coords.y);
				})
				.on("mouseout", hide_tip);
	})();

	// Barchart monthly
	(() => {
		const _data = data["monthly"].map( x => ({ 
			"month": x.month,
			"games": x.games,
			"wins": x.wins,
			"losses": x.games - x.wins
		}));

		fill(_data);

		const svg = d3.select("#chart-wins-time")
			.append("svg")
			.attr("viewBox", [0, 0, 800+20+20, 800])
			.append("g")
			.attr("transform", `translate(${margin.left+15}, ${margin.top})`);

		const x = d3.scaleBand()
			.domain(_data.map(x => x.month))
			.range([0, w])
			.padding([0.2]);

		svg.append("g")
			.attr("transform", `translate(0, ${h})`)
			.call(d3.axisBottom(x));

		const y = d3.scaleLinear()
			.domain([0, d3.max(_data, d => d.games)])
			.range([h, 0]);

		svg.append("g")
			.call(d3.axisLeft(y));

		const y_r = d3.scaleLinear()
			.domain([0, 100])
			.range([h, 0]);

		svg.append("g")
			.attr("transform", `translate(${w}, 0)`)
			.call(d3.axisRight(y_r).tickFormat(d => d + '%'));


		const color = d3.scaleOrdinal()
			.domain(['wins', 'losses'])
			.range(['#28a745', '#dc3545']);

		svg.append("text")
			.attr("text-anchor", "middle")
			.attr("x", w / 2)
			.attr("y", h + margin.top + 20)
			.text("Time (group by month)");

		svg.append("text")
			.attr("text-anchor", "middle")
			.attr("transform", "rotate(-90)")
			.attr("x", -w / 2)
			.attr("y", -margin.left)
			.text("Games")

		svg.append("text")
			.attr("text-anchor", "middle")
			.attr("transform", "rotate(90)")
			.attr("x", w / 2)
			.attr("y", - h - margin.right - margin.left + 10)
			.text("Winrate%")

		const stackedData = d3.stack()
			.keys(['wins', 'losses'])(_data)

		svg.append("g")
			.selectAll("g")
			.data(stackedData)
			.enter().append("g")
				.attr("fill", d => color(d.key))
				.selectAll("rect")
				.data(d => d)
				.enter().append("rect")
					.attr("x", d => x(d.data.month))
					.attr("y", d => y(d[1]))
					.attr("height", d => y(d[0]) - y(d[1]))
					.attr("width", x.bandwidth())
			.on("mouseover", function(d){
				tip.html(`${d.data.month}<br>W-L: <strong>${d.data.wins}-${d.data.losses}</strong><br> Winrate: <strong>${((d.data.wins/(d.data.wins+d.data.losses)) * 100).toFixed(2)}%</strong`);
				const e = d3.select(this).node().getBoundingClientRect();
	
				const coords = {
					x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
					y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
				}
				show_tip(coords.x, coords.y);
			})
			.on("mouseout", hide_tip);

		svg.append("path")
			.datum(_data)
			.attr("fill", "none")
			.attr("stroke", "#ffc107")
			.attr("stroke-width", 3)
			.attr("d", d3.line()
				.x(d => x(d.month) + x.bandwidth()/2)
				.y(d => y_r(d.wins * 100.0 / d.games))
			);
		
		svg.selectAll("circles")
			.data(_data)
			.enter()
			.append("circle")
				.attr("fill", "#ffc107")
				.attr("stroke", "none")
				.attr("cx", d => x(d.month) + x.bandwidth()/2)
				.attr("cy", d => y_r(d.wins * 100.0 / d.games))
				.attr("r", 3)
				.on("mouseover", function(d){
					tip.html(`${d.month}<br>Winrate: <strong>${(d.wins * 100.0 / d.games).toFixed(2)}%</strong>`);
					const e = d3.select(this).node().getBoundingClientRect();
		
					const coords = {
						x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
						y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
					}
					show_tip(coords.x, coords.y);
				})
				.on("mouseout", hide_tip);

	})();
}

fillForm();

submit();

$('#filters').submit(e => {
	e.preventDefault();

	submit();
});

$('form button#reset').click(e => {
	e.preventDefault();

	$('form')
		.find('select')
		.prop('selectedIndex', -1);

	$('form input').val('');

	submit();
});

function fill(data){
	for (var i = 0; i < data.length - 1; i++){
		var year = parseInt(data[i].month.slice(0, 4));
		var month = parseInt(data[i].month.slice(5));

		var next = month == 12 ? `${year+1}-01` : `${year}-${("0" + (month+1)).slice(-2)}`;

		if (data[i+1].month != next){
			data.splice(i+1, 0, {"month": next, "games": 0, "wins": 0, "losses": 0});
		}
	}
}
