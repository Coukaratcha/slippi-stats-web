import os

class Config(object):
	PLAYER_CODE=os.getenv("PLAYER_CODE")
	REPLAYS=os.getenv("REPLAYS")
	PASSWORD=os.getenv("PASSWORD")
	
	DATA=os.getenv("DATA") if os.getenv("DATA") else os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
	SECRET_KEY=os.getenv("SECRET_KEY")

	SESSION_COOKIE_NAME="slippi-stats"

	LOCAL_ONLY= True if os.getenv("LOCAL_ONLY") in ("True", "true") else False

	STATIC_FOLDER="static"
	TEMPLATES_FOLDER="templates"

	SQLALCHEMY_ECHO= True if os.getenv("SQLALCHEMY_ECHO") in ("True", "true") else False
	SQLALCHEMY_TRACK_MODIFICATIONS= True if os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS") in ("True", "true") else False
	SQLALCHEMY_DATABASE_URI = 'sqlite:///'+os.path.join(DATA, 'slippi-stats.db')
