const margin = {
	top: 80,
	right: 30,
	bottom: 30,
	left: 80
}

const w = 1000 - margin.left - margin.right;
const h = 1000 - margin.top - margin.bottom;

const tip = d3.select("body")
	.append("div")
	.attr("class", "tip");

function show_tip(x, y){
	tip
		.style("opacity", 1)
		.style("top", `${y}px`)
		.style("left", `${x}px`)
		.style("visibility", "visible");
}

function hide_tip(){
	tip
		.style("opacity", 0)
		.style("visibility", "hidden");
}

const all_characters = [
	'Dr. Mario',
	'Mario',
	'Luigi',
	'Bowser',
	'Peach',
	'Yoshi',
	'Donkey Kong',
	'Captain Falcon',
	'Ganondorf',
	'Fox',
	'Falco',
	'Ness',
	'Ice Climbers',
	'Kirby',
	'Samus',
	'Zelda',
	'Sheik',
	'Link',
	'Young Link',
	'Pichu',
	'Pikachu',
	'Jigglypuff',
	'Mewtwo',
	'Mr. Game & Watch',
	'Marth',
	'Roy'
]


function show_tip_matchup(d){
	if (d.wins + d.losses != 0){
		tip.html(`Playing <strong>${d.with}</strong> against <strong>${d.against}</strong><br>W-L: <strong>${d.wins}-${d.losses}</strong><br>`
					+ (d.wins + d.losses == 0 ? `` : `Winrate: <strong>${((d.wins/(d.wins+d.losses)) * 100).toFixed(2)}%</strong`) );
		const e = d3.select(`rect.tile[data-tile="${d.with}:${d.against}"]`).node().getBoundingClientRect();

		const coords = {
			x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
			y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
		}
		show_tip(coords.x, coords.y);
	}
}

const hide_unplayed = false;

(() => {
	const svg = d3.select("#matchups")
		.append("svg")
		.attr("viewBox", "0 0 " + (w + margin.left + margin.right) + " " + (h + margin.top + margin.bottom))
		.append("g")
		.attr("transform", "translate("+margin.left+", "+margin.top+")");

	d3.json("/api/matchups")
		.then(data => {
			const data_filtered_unplayed = hide_unplayed ? data.filter( d => (d.wins + d.losses > 0) )
													   : data;
			const opponent_char = all_characters;
			const player_char_all = data_filtered_unplayed.map(d => d.with);
			const player_char = player_char_all.filter(function(item, pos){ return player_char_all.indexOf(item) == pos;}) // unique
					.sort( (a,b) => all_characters.indexOf(a) < all_characters.indexOf(b)); // sorted by Melee order

			const x = d3.scaleBand()
				.range([0, w])
				.domain(opponent_char)
				.padding(0.2);

			const y = d3.scaleBand()
				.range([h, 0])
				.domain(player_char)
				.padding(0.2);

			const color = d3.scaleLinear()
				.range(['#488f31', '#78ab63', '#dac767', '#e18745', '#de425b'])
				.domain([1, 0.75, 0.5, 0.25, 0]);
			
			const axisX = svg.append("g")
				.attr("class", "axis")
				.call(d3.axisTop(x))
				.call(g => g.select(".domain").remove())
				.data(opponent_char)
				.selectAll(".tick")
				.append("svg:image")
				.attr("xlink:href", d => "/static/img/characters/" + d + ".png")
				.attr("transform", "translate(-12, -30)");

			const axisY = svg.append("g")
				.attr("class", "axis")
				.call(d3.axisLeft(y))
				.call(g => g.select(".domain").remove())
				.data(player_char)
				.selectAll(".tick")
				.append("svg:image")
				.attr("xlink:href", d => "/static/img/characters/" + d + ".png")
				.attr("transform", "translate(-30, -12)")

			const tile = svg.selectAll('tile')
				.data(data_filtered_unplayed)
				.enter()
				.append("rect")
				.attr("class", "tile")
				.attr("data-tile", d => `${d.with}:${d.against}`)
				.attr("x", d => x(d.against))
				.attr("y", d => y(d.with))
				.attr("width", x.bandwidth())
				.attr("height", y.bandwidth())
				.attr("fill", d => d.wins + d.losses == 0 ? "#eee" : color(d.winrate))
				.on("mouseover", show_tip_matchup)
				.on("mouseout", hide_tip);
			
			const label = svg.selectAll('label')
				.data(data_filtered_unplayed)
				.enter()
				.append("text")
				.attr("class", "label")
				.attr("x", d => x(d.against) + x.bandwidth() / 2)
				.attr("y", d => y(d.with) + y.bandwidth() / 2)
				.attr("font-size", "8")
				.attr("text-anchor", "middle")
				.text(d => d.wins + d.losses == 0 ? "" : Math.round(d.winrate * 100) + "%" )
				.on("mouseover", show_tip_matchup)
				.on("mouseout", hide_tip);

			svg.append("text")
				.attr("text-anchor", "middle")
				.attr("transform", "rotate(-90)")
				.attr("x", -h / 2)
				.attr("y", -0.5*margin.left + 0)
				.text("Player")
			svg.append("text")
				.attr("text-anchor", "middle")
				.attr("x", w / 2)
				.attr("y", -0.5*margin.top)
				.text("Opponents");
		}
	);
})();
