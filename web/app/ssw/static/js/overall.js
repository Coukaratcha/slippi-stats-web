const margin = {
	top: 15,
	bottom: 50,
	left: 30,
	right: 20
}

const w = 800 - margin.left - margin.right;
const h = 800 - margin.top - margin.bottom;

let bar_chart = true;

let data = null;

const tip = d3.select("body")
	.append("div")
	.attr("class", "tip");

function show_tip(x, y){
	tip
		.style("opacity", 1)
		.style("top", `${y}px`)
		.style("left", `${x}px`)
		.style("visibility", "visible");
}

function hide_tip(){
	tip
		.style("opacity", 0)
		.style("visibility", "hidden");
}

function show_tip_usage(character, games, totalGames, hovered){
	tip.html(	`<strong>${character}</strong><br>` +
				`<strong>${totalGames <= 0 ? "N/A" : (100*games/totalGames).toFixed(2)}%</strong><br>` +
				`${games} ${data['filter']['unique'] ? "opponent" : "game"}${games > 1 ? "s" : ""}`);
	const e = d3.select(hovered).node().getBoundingClientRect();
	const coords = {
		x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
		y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
	}
	show_tip(coords.x, coords.y);
}

function show_tip_winrate(d, hovered)
{
	tip.html(	`<strong>${d.character}</strong><br>`+ 
				`<strong>${(d.winrate ? d.winrate : 0).toFixed(2)}%</strong><br>` +
				`W-L: ${d.wins}-${d.losses}`
				);
	const e = d3.select(hovered).node().getBoundingClientRect();
	
	const coords = {
		x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
		y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
	}
	show_tip(coords.x, coords.y);
}

function character_usage_bar_chart(divName, _data)
{
	const totalGames = _data.reduce( (acc, d) => (acc + d.games), 0);
		const svg = d3.select(divName)
		.append("svg")
		.attr("viewBox", "0 0 800 800")
		.append("g")
		.attr("transform", "translate("+margin.left+", "+margin.top+")");

		const x = d3.scaleBand()
			.range([0, w])
			.domain(_data.map( d => d.character))
			.padding(0.2);

		const y = d3.scaleLinear()
			.range([h, 0])
			.domain([0, Math.max(..._data.map( d => d.games ))]);

		svg.append("g")
			.attr("class", "axis")
			.attr("transform", "translate(0, "+h+")")
			.call(d3.axisBottom(x))
			.call( g => g.select(".domain").remove() )
			.call( g => g.selectAll(".tick text, .tick line").remove())
			.data(_data.map( d => d.character ))
			.selectAll(".tick")
			.append("svg:image")
			.attr("href", d => "/static/img/characters/" + d + ".png")
			.attr("transform", "translate(-12, 12)")
			.attr("class", function(d) {
				const charData = _data.find(aD => aD.character == d);
				return (charData.games == 0 ? "gray" : ""); })
			.attr("image-rendering", "optimizeSpeed")
			.on("mouseover", function(d){
				const charData = _data.find(aD => aD.character == d);
				show_tip_usage(charData.character, charData.games, totalGames, this)
			})
			.on("mouseout", hide_tip);

		svg.append("g")
			.call(d3.axisRight(y)
				.tickSize(w))
			.call(g => g.select(".domain")
				.remove())
			.call(g => g.selectAll(".tick:not(:first-of-type) line")
				.attr("stroke-opacity", 0.5)
				.attr("stroke-dasharray", "2,2"))
			.call(g => g.selectAll(".tick text")
				.attr("x", -20)
				.attr("dy", -4)
				.style("text-anchor", "middle"));

		svg.selectAll("bar")
			.data(_data)
			.enter()
			.append("rect")
			.attr("class", "bar")
			.attr("x", d => x(d.character))
			.attr("y", d => y(d.games))
			.attr("width", x.bandwidth())
			.attr("height", d => ( h - y(d.games) ))
			.attr("fill", "#007bff")
			.on("mouseover", function(d) {show_tip_usage(d.character, d.games, totalGames, this)})
				.on("mouseout", hide_tip);
}	

function character_usage_pie_chart(divName, _data)
{
	const totalGames = _data.reduce( (acc, d) => (acc + d.games), 0);
	const minPercent = 1.0;

	var radius = Math.min(w, h) / 2 ;
	const svg = d3.select(divName)
	.append("svg")
	.attr("viewBox", "0 0 800 800")
	.append("g")
	.attr("transform", "translate("+(w/2+margin.left)+", "+(h/2+margin.top)+")")
	;

	// Compute the position of each group on the pie:
	var pie = d3.pie()
		.value(function(d) { return d.value.games; })
	var data_ready = pie(d3.entries(_data))

	// The arc generator
	var arc = d3.arc()
		.innerRadius(radius * 0.5)         // This is the size of the donut hole
		.outerRadius(radius)

	const isValid = d => (d.endAngle - d.startAngle) > (minPercent * 2.0 * Math.PI / 100.0)
	var data_with_labels = data_ready.filter( d => isValid(d) )
		
	// Filter data with percent too low, and group them in a "Others" slice
	const otherGames =  data_ready.reduce(function(sum, d) { return sum + (isValid(d) ? 0 : d.data.value.games); }, 0);
	if (otherGames > 0)
	{
		var otherData = {...data_ready[0]};
		otherData.data = {...data_ready[0].data}
		otherData.data.value = {...data_ready[0].data.value}
		otherData.data.value.character = "Others";
		otherData.data.value.games = otherGames;
		otherData.startAngle = data_with_labels.length <= 0 ? 0. : data_with_labels[data_with_labels.length - 1].endAngle;
		otherData.endAngle = 2.0 * Math.PI;

		otherData.index = data_with_labels.length;
		data_with_labels.push(otherData);
	}

	svg
		.selectAll('allSlices')
		.data(data_with_labels)
		.enter()
		.append('path')
		.attr("class", "slice")
		.attr('d', arc)
		.attr('fill', function(d){ return(char_color[d.data.value.character]) })
		.attr("stroke", "white")
		.style("stroke-width", "2px")
		.style("opacity", 0.7)
		.on("mouseover", function(d) {show_tip_usage(d.data.value.character, d.data.value.games, totalGames, this)})
		.on("mouseout", hide_tip);


	svg
		.selectAll('allLabels')
		.data(data_with_labels)
		.enter()
		//.append('text')
		//.text( function(d) { return d.data.value.character } )
		.append("svg:image")
		.attr("href", d => "/static/img/characters/" + d.data.value.character + ".png")
		.attr('transform', function(d) {
					var pos = arc.centroid(d);
					var offset = [-12,-12];
					pos[0] += offset[0];
					pos[1] += offset[1];
					return 'translate(' + pos + ')';
			})
}

function character_winrate_bar_chart(divName, _data)
{
	const totalGames = data['global']['games'];

	const svg = d3.select(divName)
		.append("svg")
		.attr("viewBox", "0 0 800 800")
		.append("g")
		.attr("transform", "translate("+margin.left+", "+margin.top+")");

	const x = d3.scaleBand()
		.range([0, w])
		.domain(_data.map( d => d.character))
		.padding(0.2);

	const y = d3.scaleLinear()
		.range([h, 0])
		.domain([0, 100]);

	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0, "+h+")")
		.call(d3.axisBottom(x))
		.call( g => g.select(".domain").remove() )
		.call( g => g.selectAll(".tick text, .tick line").remove())
		.data(_data.map( d => d.character ))
		.selectAll(".tick")
		.append("svg:image")
		.attr("href", d => "/static/img/characters/" + d + ".png")
		.attr("class", function(d) {
			const charData = _data.find(aD => aD.character == d);
			return (charData.wins + charData.losses == 0 ? "gray" : ""); })
		.attr("transform", "translate(-12, 12)")
		.attr("image-rendering", "optimizeSpeed")
		.on("mouseover", function(d){
			const charData = _data.find(aD => aD.character == d);
			show_tip_winrate(charData, this)
		})
		.on("mouseout", hide_tip);

	svg.append("g")
		.call(d3.axisRight(y)
			.tickSize(w)
			.tickFormat(d => d + '%'))
		.call(g => g.select(".domain")
			.remove())
		.call(g => g.selectAll(".tick:not(:first-of-type) line")
			.attr("stroke-opacity", 0.5)
			.attr("stroke-dasharray", "2,2"))
		.call(g => g.selectAll(".tick text")
			.attr("x", -20)
			.attr("dy", -4)
			.style("text-anchor", "middle"));
	
	svg.selectAll("bar")
			.data(_data)
			.enter()
			.append("rect")
			.attr("class", "bar")
			.attr("x", d => x(d.character))
			.attr("y", d => y(d.winrate))
			.attr("width", x.bandwidth())
			.attr("height", d => ( h - y(d.winrate) ))
			.attr("fill", "#007bff")
			.on("mouseover", function(d) {show_tip_winrate(d, this)} )
			.on("mouseout", hide_tip);
}

function submit(){
	$('#loading > .spinner-border').show();
	$('#data').hide();
	$('#no-data').hide();

	updateFilters();

	$('.match #opponent-info .code').text(filters.opponent || "?");

	$('#player-info .characters').empty();
	for(c of filters.with){
		$('#player-info .characters')
			.append($('<img>').attr("src", "/static/img/characters/"+c+".png"));
	}

	$('#opponent-info .characters').empty();
	for(c of filters.against){
		$('#opponent-info .characters')
			.append($('<img>').attr("src", "/static/img/characters/"+c+".png"));
	}
	
	$.get({
		url: "/api/overall",
		data: filters
	}).done(_data => {
		data = _data;
		update(data);
	});
}

$("#button-switch-chart-type").click(() => {
	bar_chart = !bar_chart;
	update(data);
});


const char_color = {
        'Dr. Mario' : "#E5E5E5",
        'Mario' : "#CB4030",
        'Luigi' : "#487C44",
        'Bowser' : "#D35A29",
        'Peach' : "#D46776",
        'Yoshi' : "#92F92F",
        'Donkey Kong' : "#572F1D",
        'Captain Falcon' : "#5E61BE",
        'Ganondorf' : "#4E4333",
        'Fox' : "#E38637",
        'Falco' : "#424CC8",
        'Ness' : "#E8D6A6",
        'Ice Climbers' : "#ADD6E8",
        'Kirby' : "#DD599E",
        'Samus' : "#74201E",
        'Zelda' : "#BA96F1",
        'Sheik' : "#455D8A",
        'Link' : "#68AB51",
        'Young Link' : "#C4E238",
        'Pichu' : "#FCF134",
        'Pikachu' : "#E8DD15",
        'Jigglypuff' : "#F3D3E8",
        'Mewtwo' : "#737290",
        'Mr. Game & Watch' : "#000000",
        'Marth' : "#574A75",
        'Roy' : "#390C12",
        'Others' : "#808080"
        }


function update(data){
	$('#loading > .spinner-border').hide();

	if (!data.global.games){
		$('#no-data').show();
		$('#data').hide();
		return;
	}
	
	$('#no-data').hide();
	$('#data').show();

	if (data['filter']['unique']){
		$('.global strong').first().text("Opponents");
	}
	else{
		$('.global strong').first().text("Games");
	}
	
	if (data['global']['games'] == 0){
		$('#no-data').show();
		$('.global, #charts').hide();

		return;
	}
	else{
		$('#no-data').hide();
		$('.global, #charts').show(); 
	}

	$('.global #games').text(data['global']['games']);
	$('.global #wins').text(data['global']['wins']);
	$('.global #losses').text(data['global']['losses']);
	$('.global #ties').text(data['global']['ties']);
	$('.global #winrate').text(data['global']['winrate'] != null ? data['global']['winrate'].toFixed(2) + "%" : "N/A");
	
	const charts = [
		"#chart-characters-usage",
		"#chart-opponents-usage",
		"#chart-characters-winrate",
		"#chart-opponents-winrate",
		"#chart-stages-usage",
		"#chart-stages-winrate",
		"#chart-usage-winrate"
	];

	for (c of charts){
		$(c).children("svg").remove();
	}

	// characters-usage
	(() => {
		const _data = data["with"].sort((a, b) => b.games - a.games)

		if (bar_chart)
		{
			character_usage_bar_chart("#chart-characters-usage", _data);			
		}
		else
		{
			character_usage_pie_chart("#chart-characters-usage", _data)
		}
	})();

	// characters-winrate
	(() => {
		const _data = data["with"].sort((a, b) => b.winrate - a.winrate || b.losses - a.losses)
		character_winrate_bar_chart("#chart-characters-winrate", _data)
	})();

	// opponents-usage
	(() => {
		const _data = data["against"].sort((a, b) => b.games - a.games)

		if (bar_chart)
		{
			character_usage_bar_chart("#chart-opponents-usage", _data);			
		}
		else
		{
			character_usage_pie_chart("#chart-opponents-usage", _data)
		}
	})();

	// opponents-winrate
	(() => {
		const _data = data["against"].sort((a, b) => b.winrate - a.winrate || b.losses - a.losses)
		character_winrate_bar_chart("#chart-opponents-winrate", _data)
	})();

	// stages-usage
	(() => {
		const _data = data["stages"].sort((a, b) => b.games - a.games)

		const svg = d3.select("#chart-stages-usage")
			.append("svg")
			.attr("viewBox", "0 0 800 800")
			.append("g")
			.attr("transform", "translate("+margin.left+", "+margin.top+")");

		const x = d3.scaleBand()
			.range([0, w])
			.domain(_data.map( d => d.stage))
			.padding(0.2);

		const y = d3.scaleLinear()
			.range([h, 0])
			.domain([0, Math.max(..._data.map( d => d.games ))]);

		svg.append("g")
			.attr("transform", "translate(0, "+h+")")
			.call(d3.axisBottom(x))
			.call( g => g.select(".domain").remove() )
			.call( g => g.selectAll(".tick line").remove())
			.selectAll("text")
			.style("text-anchor", "middle");

		svg.append("g")
			.call(d3.axisRight(y)
				.tickSize(w))
			.call(g => g.select(".domain")
				.remove())
			.call(g => g.selectAll(".tick:not(:first-of-type) line")
				.attr("stroke-opacity", 0.5)
				.attr("stroke-dasharray", "2,2"))
			.call(g => g.selectAll(".tick text")
				.attr("x", -20)
				.attr("dy", -4)
				.style("text-anchor", "middle"));

		svg.selectAll("bar")
			.data(_data)
			.enter()
			.append("rect")
			.attr("class", "bar")
			.attr("x", d => x(d.stage))
			.attr("y", d => y(d.games))
			.attr("width", x.bandwidth())
			.attr("height", d => ( h - y(d.games) ))
			.attr("fill", "#007bff")
			.on("mouseover", function(d){
				tip.html(`<strong>${d.stage}</strong><br><strong>${d.games}</strong> ${data['filter']['unique'] ? "opponents" : "games"}`);
				const e = d3.select(this).node().getBoundingClientRect();
	
				const coords = {
					x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
					y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
				}
				show_tip(coords.x, coords.y);
			})
			.on("mouseout", hide_tip);
	})();

	// stage-winrate
	(() => {
		const _data = data["stages"].sort((a, b) => b.winrate - a.winrate || b.losses - a.losses)

		const svg = d3.select("#chart-stages-winrate")
			.append("svg")
			.attr("viewBox", "0 0 800 800")
			.append("g")
			.attr("transform", "translate("+margin.left+", "+margin.top+")");

		const x = d3.scaleBand()
			.range([0, w])
			.domain(_data.map( d => d.stage))
			.padding(0.2);

		const y = d3.scaleLinear()
			.range([h, 0])
			.domain([0, 100]);

		svg.append("g")
			.attr("transform", "translate(0, "+h+")")
			.call(d3.axisBottom(x))
			.call( g => g.select(".domain").remove() )
			.call( g => g.selectAll(".tick line").remove())
			.selectAll("text")
			.style("text-anchor", "middle");

		svg.append("g")
			.call(d3.axisRight(y)
				.tickSize(w)
				.tickFormat(d => d + '%'))
			.call(g => g.select(".domain")
				.remove())
			.call(g => g.selectAll(".tick:not(:first-of-type) line")
				.attr("stroke-opacity", 0.5)
				.attr("stroke-dasharray", "2,2"))
			.call(g => g.selectAll(".tick text")
				.attr("x", -20)
				.attr("dy", -4)
				.style("text-anchor", "middle"));

		svg.selectAll("bar")
			.data(_data)
			.enter()
			.append("rect")
			.attr("class", "bar")
			.attr("x", d => x(d.stage))
			.attr("y", d => y(d.winrate))
			.attr("width", x.bandwidth())
			.attr("height", d => ( h - y(d.winrate) ))
			.attr("fill", "#007bff")
			.on("mouseover", function(d){
				tip.html(	`<strong>${d.stage}</strong><br><strong>${d.winrate.toFixed(2)}%</strong><br>`+
							`W-L: ${d.wins}-${d.losses}`);


				const e = d3.select(this).node().getBoundingClientRect();
	
				const coords = {
					x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
					y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
				}
				show_tip(coords.x, coords.y);
			})
			.on("mouseout", hide_tip);
	})();

	// usage-winrate
	(() => {
		const totalGames = data['global']['games'];

		const _data = data["against"]
			.filter(d => d.games > 0);

		const svg = d3.select("#chart-usage-winrate")
			.append("svg")
				.attr("viewBox", `0 0 ${w + margin.left + margin.right + 20 + 20} ${h + margin.top + margin.bottom}`)
			.append("g")
				.attr("transform", `translate(${margin.left + 20}, ${margin.top})`);

			const x = d3.scaleLinear()
				.domain([0, Math.ceil(Math.max(..._data.map( d => d.games * 100.0 / totalGames )))])
				.range([0, w]);

			const y = d3.scaleLinear()
				.domain([0, 100])
				.range([h, 0]);
			
			svg.append("g")
				.attr("transform", `translate(0, ${h})`)
				.call(d3.axisBottom(x).tickFormat(d => d + '%'));

			svg.append("text")
				.attr("text-anchor", "middle")
				.attr("x", w / 2)
				.attr("y", h + margin.top + 20)
				.text("Opponent's usage of characters");

			svg.append("g")
				.call(d3.axisLeft(y).tickFormat(d => d + '%'))
				.selectAll(".tick:not(:first-of-type)")
				.append("line")
					.attr("stroke", "black")
					.attr("x1", 0)
					.attr("x2", w)
					.attr("stroke-opacity", 0.5)
					.attr("stroke-dasharray", "2,2");
			
			svg.append("g")
				.attr("transform", `translate(${w}, 0)`)
				.call(d3.axisRight(y).tickFormat(d => d + '%'));

			svg.append("text")
				.attr("text-anchor", "middle")
				.attr("transform", "rotate(-90)")
				.attr("x", -w / 2)
				.attr("y", -margin.left + 0)
				.text("Winrate%")

			svg.append("g")
				.append("line")
					.attr("stroke", "#ffc107")
					.attr("stroke-width", "3")
					.attr("x1", 0)
					.attr("x2", w)
					.attr("y1", y(data['global']['winrate']))
					.attr("y2", y(data['global']['winrate']))
					.on("mouseover", function(d){
						tip.html(`<span class="text-warning">Average winrate</span>: <strong>${data['global']['winrate'].toFixed(2)}%</strong>`);
						const e = d3.select(this).node().getBoundingClientRect();
	
						const coords = {
							x: svg.node().getBoundingClientRect().right - 200 + window.pageXOffset,
							y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
						}
						show_tip(coords.x, coords.y);
					})
					.on("mouseout", hide_tip);

			svg.append("g")
				.selectAll("dot")
				.data(_data)
				.enter()
				.append("svg:image")
				.attr("class", "char")
				.attr("href", d => "/static/img/characters/" + d.character + ".png")
				.attr("x", d => x(d.games * 100.0 / totalGames) - 12)
				.attr("y", d => y(d.winrate) - 12)
				.attr("image-rendering", "optimizeSpeed")
				.on("mouseover", function(d){
					tip.html(`<strong>${d.character}</strong><br>Winrate: <strong>${d.winrate.toFixed(2)}%</strong><br>Usage: <strong>${(d.games * 100.0 / totalGames).toFixed(2)}%</strong>`);
					const e = d3.select(this).node().getBoundingClientRect();
	
					const coords = {
						x: e.x - tip.node().getBoundingClientRect().width / 2 + e.width / 2 + window.pageXOffset,
						y: e.y - tip.node().getBoundingClientRect().height - 10  + window.pageYOffset
					}
					show_tip(coords.x, coords.y);
				})
				.on("mouseout", hide_tip);
	})();
}

fillForm();
submit();

$('form#filters').submit(e => {
	e.preventDefault();

	submit();
});

$('form button#reset').click(e => {
	e.preventDefault();

	$('form')
		.find('select')
		.prop('selectedIndex', -1);

	$('form input[type="text"]').val('');
	$('form input[type="checkbox"]').prop('checked', false);

	submit();
});
