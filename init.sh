#!/bin/bash

# Create folders for volumes (Docker-Compose does it automatically, Podman-Compose does not)
mkdir -p replays/ data/

# Duplicate conf files for instance
cp production.env.sample production.env
