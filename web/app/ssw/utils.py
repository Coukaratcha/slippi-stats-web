from functools import wraps
from flask import (
	abort,
	session,
	current_app
)

def require_login(view_function):
	@wraps(view_function)
	def decorated_function(*args, **kwargs):
		if current_app.config["LOCAL_ONLY"] or "login" in session:
			return view_function(*args, *kwargs)
		else:
			abort(403, "You need to be logged in")
	return decorated_function