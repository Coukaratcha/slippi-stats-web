from flask import (
	current_app
)

import os
import slippi
import threading

from ssw.models.game import Game
from ssw.database import db
from ssw.const import (
	characters,
	CSSCharacter,
	stages
)

EXCLUDE_MIN_SIZE = 630000

class ImportingThread(threading.Thread):
	def __init__(self):
		self.exclude = False
		self.progress = {
			"running": False,
			"total": 0,
			"ignored": 0,
			"errors": 0,
			"imported": 0,
			"log": []
		}
		self.lock = False
		super().__init__()

	def run(self):
		self.lock = True
		replays = current_app.config['REPLAYS']
		player_code = current_app.config['PLAYER_CODE']
		player = {
			"id": None,
			"score": None,
			"code": player_code,
			"character": None,
			"damage": None
		}
		opponent = {
			"id": None,
			"score": None,
			"code": None,
			"name": None,
			"character": None,
			"damage": None
		}

		date = None
		win = None
		stage = None
		duration = None

		i = 0

		list_files = []

		for root, dirs, files in os.walk(replays):
			for f in files:
				if f.endswith('.slp'):
					list_files.append((root, f))

		self.progress = {
			"running": True,
			"total": len(list_files),
			"ignored": 0,
			"errors": 0,
			"imported": 0,
			"log": []
		}

		for r, f in list_files:
			g = Game.query.filter_by(filename=f).first()

			if g is not None:
				self.progress['ignored'] += 1
				self.progress["log"].append(("INFO", "{f} was already imported".format(f=f)))
				continue

			if self.exclude:
				try:
					size = os.stat(os.path.join(r, f)).st_size

					if size < EXCLUDE_MIN_SIZE:
						self.progress["ignored"] += 1
						self.progress["log"].append(("INFO", "{f} was ignored as file's size is less than limit ({l} bytes)".format(f=f, l=EXCLUDE_MIN_SIZE)))
						continue
				except:
					self.progress['errors'] += 1
					self.progress["log"].append(("ERROR", "{f} was not found (fullpath: {p})".format(f=f, p=os.path.join(r, f))))

			try:
				d = slippi.Game(os.path.join(r, f))
			except:
				self.progress['errors'] += 1
				self.progress['log'].append(("ERROR", "Cannot parse {f}".format(f=f)))
				continue

			nb_players = len([p for p in d.metadata.players if p is not None])

			if nb_players != 2:
				self.progress['errors'] += 1
				self.progress['log'].append(("ERROR", "{f} : {nb} players were found instead of exactly 2 needed".format(f=f, nb=nb_players)))
				continue

			end = d.frames[-1]

			if None in [p.netplay for p in d.metadata.players if p is not None]:
				self.progress['errors'] += 1
				self.progress['log'].append(("ERROR", "At least one player has no netplay's code in {f}".format(f=f)))
				continue

			player['id'] = None;
			for i in range(0, len(d.metadata.players)):
				if d.metadata.players[i] is not None:
					if d.metadata.players[i].netplay.code == player_code:
						player['id'] = i
					else:
						opponent['id'] = i
						opponent['code'] = d.metadata.players[i].netplay.code
						opponent['name'] = d.metadata.players[i].netplay.name

			if player['id'] is None:
				self.progress['errors'] += 1
				self.progress["log"].append(('ERROR', "{p} was not found among players in {f}".format(p=player_code, f=f)))
				continue

			player['score'] = 4 - end.ports[opponent['id']].leader.post.stocks
			player['damage'] = end.ports[player['id']].leader.post.damage

			if d.start.players[player['id']].character not in CSSCharacter or d.start.players[opponent['id']].character not in CSSCharacter:
				self.progress['errors'] += 1
				self.progress['log'].append(('ERROR', 'At least one player uses an illegal character in {f}'.format(f=f)))
				continue

			player['character'] = CSSCharacter[d.start.players[player['id']].character]
			opponent['score'] = 4 - end.ports[player['id']].leader.post.stocks
			opponent['damage'] = end.ports[opponent['id']].leader.post.damage
			opponent['character'] = CSSCharacter[d.start.players[opponent['id']].character]

			date = d.metadata.date
			duration = d.metadata.duration

			if d.start.stage not in stages:
				self.progress["errors"] += 1
				self.progress["log"].append(("ERROR", "{f} was played on an illegal stage".format(f=f)))
				continue

			stage = stages[d.start.stage]

			if not d.end:
				self.progress['errors'] += 1
				self.progress["log"].append(('ERROR', "There is no end game in {f}".format(f=f)))
				continue

			win = None
			if d.end.method in (slippi.event.End.Method.GAME, slippi.event.End.Method.TIME):
				if player['score'] > opponent['score']:
					win = True
				elif player['score'] < opponent['score']:
					win = False
				else:
					if player['damage'] < opponent['damage']:
						win = True
					elif player['damage'] > opponent['damage']:
						win = False

			g = Game(
				filename=f, 
				status="00", 
				datetime=date, 
				opponent=opponent['code'],
				opponent_nickname=opponent['name'], 
				char_player=player['character'], 
				char_opponent=opponent['character'], 
				score_player=player['score'], 
				score_opponent=opponent['score'], 
				win=win, 
				stage=stage,
				duration=duration
			)

			db.session.add(g)
			db.session.commit()

			self.progress['imported'] += 1
			self.progress['log'].append(('SUCCESS', '{f} imported successfully'.format(f=f)))

		self.progress["running"] = False
		self.lock = False
