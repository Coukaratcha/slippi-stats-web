let searchParams = new URLSearchParams(window.location.search)

let filters;

if (!window.sessionStorage.getItem("filters")){
	filters = {
		opponent: null,
		date_before: null,
		date_after: null,
		against: [],
		with: [],
		stages: [],
		unique: false
	};
}
else{
	filters = JSON.parse(window.sessionStorage.getItem("filters"));
}

if (searchParams.has("req")){
	filters = JSON.parse(searchParams.get("req"));
}

window.sessionStorage.setItem("filters", JSON.stringify(filters))

function fillForm(){
	$('form#filters input#opponent').val(filters.opponent);
	$('form#filters input#date_before').val(filters.date_before);
	$('form#filters input#date_after').val(filters.date_after);
	$('form#filters select#with').val(filters.with);
	$('form#filters select#against').val(filters.against);
	$('form#filters select#stages').val(filters.stages);
	$('form#filters input#unique').prop("checked", filters.unique);
}

function updateFilters(){
	filters.opponent = $('form#filters input#opponent').val();
	filters.date_before = $('form#filters input#date_before').val();
	filters.date_after = $('form#filters input#date_after').val();
	filters.with = $('form#filters select#with').val();
	filters.against = $('form#filters select#against').val();
	filters.stages = $('form#filters select#stages').val();
	filters.unique = $('form#filters input#unique').prop("checked");
	window.sessionStorage.setItem("filters", JSON.stringify(filters));
}